# marko-cpp-starter-kit

# Starter kit for CPP projects.

Includes support for unit testing via GoogleTest library as well as boost library to get you up and running quickly.


# Building

1. Clone repository
1. mkdir build && cd build
1. cmake -GNinja .. "-DCMAKE_TOOLCHAIN_FILE=/home/mvuksano/workspace/vcpkg-export/scripts/buildsystems/vcpkg.cmake"
1. ninja


# Testing
1. run `main_test` binary

# Using VS Code via remote ssh
Ensure the following extensions are installed:
1. Remote - SSH (installed in your local VS code instance)
1. C++ ( install on remote host )
1. CMake Tools ( install on remote host )

Opening this project should do the above steps for you automatically. This can be modified via `.vscode/settings.json` file.

Add toolchain configuration to your `cmake-tools-kits.json` file.

```json
  {
    "name": "VCPKG toolchain",
    "toolchainFile": "__PATH_TO_WORKSPACE__/vcpkg-export/scripts/buildsystems/vcpkg.cmake"
  }
```

Where `__PATH_TO_WORKSPACE__` is path where workspace folder is located (e.g. `/home/ubuntu/workspace`).
